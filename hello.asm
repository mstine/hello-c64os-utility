;----[ hello.asm ]-----------------------

         .include "os/h/modules.h"

         #inc_s "app"
         #inc_s "pointer"

         #inc_s "util.frame"

         #inc_s "memory"
         #inc_s "screen"
         #inc_s "service"
         #inc_s "string"
         #inc_s "toolkit"

         #inc_tkh "classes"
         #inc_tks "tksizes"

         #inc_tkh "tkobj"
         #inc_tkh "tklabel"
         #inc_tkh "tkctrl"
         #inc_tkh "tkbutton"

         #inc_tks "tkview"
         #inc_tks "tkbutton"

         *= utilbase

;----[ Vector Table ]-------------------
        .word identity
        .word initutil
        .word quitutil
        .word sec_rts   ;msgcmd

;----[ Utility Framework Vector Table ]-
        .word scrlayer
        .word drawctx
        .word tkenv
        .word config
        .byte 3         ;Config Size

;----[ Utility State ]------------------

;----[ View Store ]---------------------
views    .word 0,0 ; 2 view pointers

;----[ Identity ]-----------------------
identity .null "Hello World"

;----[ Screen Layer ]-------------------
scrlayer .word uf_draw ; UFW Draw
         .word uf_pmus ; UFW Mouse Evts
         .word uf_kcmd ; UFW KeyCmd Evts
         .word uf_kprt ; UFW KeyPrnt Ents
         .byte 0       ; Scrn Layer Idx

;----[ Draw Context ]-------------------
drawctx  .word charmap  ; Char Origin
         .word colmap   ; Color Origin
         .byte 20       ; Buffer Width
         .byte 20       ; Width
         .byte 6        ; Height
         .word 0        ; Offset Top
         .word 0        ; Offset Left

;----[ Toolkit Environment ]------------
tkenv    .word drawctx
         .byte 0        ; mempool
         .byte 1        ; starts dirty
         .byte 0        ; layer
         .word 0        ; rootview
         .word 0        ; fkeyv
         .word 0        ; fmusv
         .word 0        ; cmusv
         ;posx
         ;posy

;----[ Config Data Block ]--------------
config
         posx     .byte 8 ; start x pos
         posy     .byte 4 ; start y pos
         toggle   .byte 2 ; tgls 2/-2
         lblidx   .byte 0 ; lbl str idx
         btnidx   .byte 1 ; btn str idx

;----[ Content Data Block ]-------------
lblstr
    #straxget
    .null "Hello, World!"   ;0
    .null "Goodbye, World!" ;1

btnstr
    #strxyget
    .null "Say Goodbye"     ;0
    .null "Say Hello"       ;1
;---------------------------------------

ptr      = $fb; $fc

quitutil
         .block
         ;Free mempool
         ;Y -> Page address to free
         ldy tkenv+te_mpool 
         ;X -> Number of pages to free
         ldx #1             
         jsr pgfree

         jmp uf_quit
         .bend

initutil
         .block
         ; Create KERNAL module jmp tbl
         ; RegPtr -> ptr to extern tbl
         #ldxy extern       
         jsr initextern

         ; Load uf_init config flags
         ; uf_cnflb  = 
         ; use global config file
         ; uf_settit = 
         ; draw std title bar
         ; uf_clrbuf = 
         ; clear char/color buffer
         lda #uf_cnfglb.uf_settit
         ora #uf_clrbuf

         ; Execute UFW init
         jsr uf_init

         ; Allocate mempool
         ; A -> Allocation type
         lda #maputil ; by Utility
         ; X -> Pages to allocate
         ldx #1
         jsr pgalloc
         ; Y -> Ptr to 1st Page
         sty tkenv+te_mpool

         ; Set Toolkit environment
         ; RegPtr -> ptr to tkenv
         #ldxy tkenv
         jsr settkenv

         ; Instantiate TKView
         ldx #tkview
         jsr classptr
         jsr tknew

         ; Set new TKView object as root view
         #stxy tkenv+te_rview

         ; Execute TKView "constructor"
         ldy #init_
         jsr getmethod
         jsr sysjmp

         ; Set view offsets from draw context
         #setobj8 this,offtop,2
         #setobj8 this,offleft,1
         #setobj8 this,offrght,1

         ; Instantiate TKLabel
         ldx #tklabel
         jsr classptr
         jsr tknew

         ; Stash pointer for later toggling
         #storeset views,0

         ; Execute TKLabel "constructor"
         ldy #init_
         jsr getmethod
         jsr sysjmp

         #setflag this,dflags,df_opaqu

         ; Set TKLabel string pointer
         ldy #setstrp_
         jsr getmethod
         ; A -> index of label 
         lda toggle
         jsr lblstr
         ; AX -> contains string pointer
         jsr sysjmp

         ; Append to root view
         #rdxy tkenv+te_rview
         jsr appendto

         ; Instantiate TKButton
         ldx #tkbutton
         jsr classptr
         jsr tknew

         ; Stash pointer for later toggling
         #storeset views,1

         ; Execute TKButton "constructor"
         ldy #init_
         jsr getmethod
         jsr sysjmp

         #setobj8 this,rsmask,rm_ankl.rm_ankb
         #setobj8 this,width,13
         #setobj8 this,lpad,1

         ; Set TKButton title
         ldy #settitle_
         jsr getmethod
         ; A -> index of title
         lda toggle
         jsr btnstr
         ; RegPtr -> pointer to "Say Goodbye"
         jsr sysjmp

         ; Set TKButton target
         ldy #settgt_
         jsr getmethod
         ; A -> 0 ~> not OO routine
         lda #0
         ; RegPtr -> tgt routine ptr
         #ldxy onclick
         jsr sysjmp

         ; Append to root view
         #rdxy tkenv+te_rview
         jmp appendto
         .bend
        
onclick
         .block
         lda toggle
         eor #1
         sta toggle

         ldy #settitle_
         jsr getmethod
         lda toggle
         jsr btnstr
         jsr sysjmp

         #setflag this,dflags,df_dirty

         ; Make 'this' the TKLabel
         #storeget views,0
         jsr ptrthis

         ldy #setstrp_
         jsr getmethod
         lda toggle
         jsr lblstr
         jsr sysjmp

         #setflag this,dflags,df_dirty

         jmp tkupdate
         .bend

;---------------------------------------

extern
          #inc_h "memory"

pgalloc   #syscall lmem,pgalloc_ 
pgfree    #syscall lmem,pgfree_

          #inc_h "string"

pet2scr   #syscall lstr,pet2scr_

          #inc_h "toolkit"

settkenv  #syscall ltkt,settkenv_
classptr  #syscall ltkt,classptr_
ptrthis   #syscall ltkt,ptrthis_
tknew     #syscall ltkt,tknew_
getmethod #syscall ltkt,getmethod_
appendto  #syscall ltkt,appendto_
tkupdate  #syscall ltkt,tkupdate_

         .byte $ff ;Terminator

;----[ Character Map ]------------------

charmap  .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60

         *= *+(25*5) ; 5 lines of $a0

;----[ Color Map ]----------------------
colmap   .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60,$60,$60,$60,$60
         .byte $60,$60

         ; 5 lines of $42 - c_fpanel
