# Hello C64 OS Utility

"Hello" is intended to demonstrate the very basics of [C64 OS](https://c64os.com/) [Utility](https://c64os.com/c64os/usersguide/utilities) development in MOS 6502 machine language for the Commodore 64.

Its feature set is very basic. Upon launch, it presents the message, "Hello, World!" along with a button labeled "Say Goodbye."

![Say Goodbye](images/saygoodbye.png)

Upon clicking the button, the message is toggled to "Goodbye, World!" and the button is toggled to "Say Hello." The Utility remembers its previous state between executions.

![Say Hello](images/sayhello.png)

The code is thoroughly commented, and a walk-through blog post is forthcoming at https://mattstine.com.

## Dependencies

* You'll need an installation of Turbo Macro Pro's multiplatform cross assembler, [TMPx](https://style64.org/release/tmpx-v1.1.0-style).
* You'll also need the [C64 OS Headers](https://github.com/gnacu/c64os-dev) for cross assembly. TMPx has an argument, `-I` that accepts directory paths to add to the `.include` search path, but it doesn't seem to be working. However, if you copy or symlink the root of the `os` tree found in the linked repo to the same directory as `hello.asm`, everything will work.
* If you want to build the D64 disk image, you'll also need an installation of [VICE](https://vice-emu.sourceforge.io/).

## Building

The easiest path is to use the included `Makefile`. 
Assuming you've added `tmpx` to your `PATH`, you should be able to execute:

`$ make Hello`

The `Makefile` uses VICE's `c1541` tool to produce the image. Running:

`$ make Hello.d64`

...will both assemble the Utility and create the image. Of course, you can also just download the latest build artifact from the [Pipelines](https://gitlab.com/mstine/hello-c64os-utility/-/pipelines) page. Just click on the download icon to the right of the latest passing pipeline. You'll get a nested set of ZIP files containing the D64. 

![Made for C64 OS](images/Made-For-C64-OS-x4.png)