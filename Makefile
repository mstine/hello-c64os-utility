Hello.zip: Hello.d64
	zip Hello.zip Hello.d64

Hello.d64: Hello
	c1541 -format "hello,42" d64 Hello.d64
	c1541 -attach Hello.d64 -write Hello Hello

Hello: hello.asm
	tmpx -i hello.asm -o Hello

clean:
	rm -f Hello.zip
	rm -f Hello.d64
	rm -f Hello
